#!/usr/bin/env python
# coding: utf-8

import numpy as np 
import pandas as pd
import time
import os.path
from openpyxl import Workbook
from sklearn.cluster import KMeans
from sklearn.cluster import MeanShift


# values: a two-dimensional array, m number of n-dimensional vectors to be clustered;
def modified_kmeans_cluster(values_to_cluster, threshold, k_start, n_clusters=None):
    if n_clusters is not None:
        kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(values_to_cluster)
        return  kmeans.labels_
    else:
        n_clusters = k_start
        n_values = len(values_to_cluster)
        assert n_values > 0
        kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(values_to_cluster)
        inertias = [kmeans.inertia_]
        while n_values > n_clusters:
            n_clusters_new = n_clusters + 1
            kmeans_new = KMeans(n_clusters=n_clusters_new, random_state=0).fit(values_to_cluster)
            inertias.append(kmeans_new.inertia_)
            if terminate_clustering(inertias, threshold):
                break
            kmeans = kmeans_new
            n_clusters += 1
        return kmeans.labels_


def terminate_clustering(inertias, threshold):
    # method: compute relative improvement toward previous step
    assert len(inertias) > 1
    improvement = 1 - (inertias[-1] / inertias[-2])
    return improvement < threshold




def cluster_existed_features(network_folder_path, classes, layers_indexes, taus):
    appendixes = ["_bad_high_level_features.csv", "_good_high_level_features.csv"]
    product = ((i, y, appendix) for i in layers_indexes for y in classes for appendix in appendixes)
    
    for i, y, appendix in product:
        start_time = time.time()
        # load data for class y at layer i
        features_file_path = network_folder_path +"Layer_" + str(i) + "/class_" + str(y) + appendix
        values_to_cluster = np.genfromtxt(features_file_path, delimiter=',')
        
        if len(values_to_cluster):
            # specify path and then load existing clustering results
            k_and_taus = dict()
            taus_existed = []
            clustering_results = pd.DataFrame()
            clustering_results_path = network_folder_path + "Layer_" + str(i) + "/clustering_results_class_" + str(y) + appendix

            if os.path.exists(clustering_results_path):
                clustering_results = pd.read_csv(clustering_results_path)
                for col in clustering_results.columns:
                    k_and_taus[col] = clustering_results[col].max() + 1

            # update the existing values of tau
            taus_existed = [float(key) for key in k_and_taus.keys()]

            # remove existing tau from list existed_taus
            taus_new = [tau for tau in taus if tau not in taus_existed]

            # iterate every tau to cluster the given data
            for tau in taus_new:
                # fix starting searching point
                k_start = 1
                bigger_taus = [x for x in taus_existed if x > tau]
                if len(bigger_taus):
                    tau_closest = min(bigger_taus) 
                    k_start = k_and_taus[str(tau_closest)]

                # start to cluster
                cluster_labels = modified_kmeans_cluster(values_to_cluster, tau, k_start)
                clustering_results[str(tau)] = cluster_labels
                taus_existed.append(tau)
                k_and_taus[str(tau)] = max(cluster_labels) + 1

            clustering_results.to_csv(clustering_results_path, index = False)
            elapsed_time = time.time() - start_time
            print("file:" + "layer_" + str(i) + "_class_" + str(y) + appendix + ",", "lasting time:", elapsed_time, "seconds")



def instance_0_MNIST():
    network_folder_path = "./models/MNIST/"
    #stored_network_name = "CNN_MNIST_0-9/"
    classes = range(10)
    layers_indexes = [9, 8, 7, 6]
    taus = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0.01]
    cluster_existed_features(network_folder_path, classes, layers_indexes, taus)

# instance_0_MNIST()


def instance_1_F_MNIST():
    network_folder_path = "./models/F_MNIST/"
    #stored_network_name = "CNN_F_MNIST_0-9/"
    classes = range(10)
    layers_indexes = [9, 8, 7, 6]
    taus = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0.01]
    cluster_existed_features(network_folder_path, classes, layers_indexes, taus)

# instance_1_F_MNIST()


def instance_2_CIFAR10():
    network_folder_path = "./models/CIFAR10/"
    #stored_network_name = "CNN_MNIST_0-9/"
    classes = range(10)
    layers_indexes = [11, 10, 9, 8]
    taus = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0.01]
    cluster_existed_features(network_folder_path, classes, layers_indexes, taus)

# instance_2_CIFAR10()



def instance_3_GTSRB():
    network_folder_path = "./models/GTSRB/"
    #stored_network_name = "CNN_MNIST_0-9/"
    classes = range(43)
    layers_indexes = [11, 10, 9, 8]
    taus = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0.01]
    cluster_existed_features(network_folder_path, classes, layers_indexes, taus)

# instance_3_GTSRB()





