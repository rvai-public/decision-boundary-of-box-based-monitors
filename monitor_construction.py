#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from Abstractions import *
from RuntimeMonitors import *
import os
import pickle

# model_path = "{}_{}.h5".format(stored_network_name, classes_string)
# model = keras.models.load_model('../models/'+model_path)

def monitors_offline_construction(network_name, network_folder_path, classes, layers_indexes, taus):
    appendixes = ["_bad_high_level_features.csv", "_good_high_level_features.csv"]
    product = ((i, y) for i in layers_indexes for y in classes)

    for i, y in product:
        
        # load obtained features to creat reference
        path_bad_features = network_folder_path +"Layer_" + str(i) + "/class_" + str(y) + appendixes[0]
        path_good_features = network_folder_path +"Layer_" + str(i) + "/class_" + str(y) + appendixes[1]
        bad_feat_clustering_results = []
        good_feat_clustering_results = []


        if os.path.exists(path_bad_features):
            bad_features_to_cluster = np.genfromtxt(path_bad_features, delimiter=',')
            # load clustering results to partition the features
            bad_feat_clustering_results_path = network_folder_path + "Layer_" + str(i) + "/clustering_results_class_" + str(y) + appendixes[0]
            if os.path.exists(bad_feat_clustering_results_path):
                bad_feat_clustering_results = pd.read_csv(bad_feat_clustering_results_path)

        if os.path.exists(path_good_features):
            good_features_to_cluster = np.genfromtxt(path_good_features, delimiter=',')
            # load clustering results to partition the features
            good_feat_clustering_results_path = network_folder_path + "Layer_" + str(i) + "/clustering_results_class_" + str(y) + appendixes[1]
            n_dim = good_features_to_cluster.shape[1]
            if os.path.exists(bad_feat_clustering_results_path):
                good_feat_clustering_results = pd.read_csv(good_feat_clustering_results_path)

        for tau in taus:
            good_loc_boxes = []
            bad_loc_boxes = []

            if len(bad_feat_clustering_results):
                # load clustering result related to tau
                bad_feat_clustering_result = bad_feat_clustering_results[str(tau)]
                # determine the labels of clusters
                bad_num_clusters = np.amax(bad_feat_clustering_result) + 1
                bad_clustering_labels = np.arange(bad_num_clusters)
                
                # extract the indices of vectors in a cluster
                bad_clusters_indices = []
                for k in bad_clustering_labels:
                    bad_indices_cluster_k, = np.where(bad_feat_clustering_result == k)
                    bad_clusters_indices.append(bad_indices_cluster_k)
                
                # creat local box for each cluster
                bad_loc_boxes = [Box() for i in bad_clustering_labels]
                for j in range(len(bad_loc_boxes)):
                    bad_loc_boxes[j].build(n_dim, bad_features_to_cluster[bad_clusters_indices[j]])
                

            if len(bad_feat_clustering_results):
                # load clustering result related to tau
                good_feat_clustering_result = good_feat_clustering_results[str(tau)]
                # determine the labels of clusters 
                good_num_clusters = np.amax(good_feat_clustering_result) + 1
                good_clustering_labels = np.arange(good_num_clusters)
                
                # extract the indices of vectors in a cluster
                good_clusters_indices = []
                for k in good_clustering_labels:
                    good_indices_cluster_k, = np.where(good_feat_clustering_result == k)
                    good_clusters_indices.append(good_indices_cluster_k)    
                
                # creat local box for each cluster
                good_loc_boxes = [Box() for i in good_clustering_labels]
                for j in range(len(good_loc_boxes)):
                    good_loc_boxes[j].build(n_dim, good_features_to_cluster[good_clusters_indices[j]])

            # creat the monitor for class y at layer i
            monitor_y_i = Monitor(network_name, y, i, good_ref=good_loc_boxes, bad_ref=bad_loc_boxes)
            monitor_stored_path = network_folder_path + "Monitors/" + network_name + "_monitor_for_class_" + str(y) + "_at_layer_" + str(i) + "_tau_" + str(tau) + ".pkl"
            with open(monitor_stored_path, 'wb') as f:
                pickle.dump(monitor_y_i, f)


def instance_0_monitors_construction_MNIST():
    network_folder_path = "./models/MNIST/"
    network_name = "CNN_MNIST_0-9"
    classes = range(10)
    layers_indexes = [9, 8, 7, 6]
    taus = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0.01]
    monitors_offline_construction(network_name, network_folder_path, classes, layers_indexes, taus)


def instance_1_monitors_construction_F_MNIST():
    network_folder_path = "./models/F_MNIST/"
    network_name = "CNN_F_MNIST_0-9"
    classes = range(10)
    layers_indexes = [9, 8, 7, 6]
    taus = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0.01]
    monitors_offline_construction(network_name, network_folder_path, classes, layers_indexes, taus)


def instance_2_monitors_construction_CIFAR10():
    network_folder_path = "./models/CIFAR10/"
    network_name = "CNN_CIFAR10_0-9"
    classes = range(10)
    layers_indexes = [11, 10, 9, 8]
    taus = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0.01]
    monitors_offline_construction(network_name, network_folder_path, classes, layers_indexes, taus)


def instance_3_monitors_construction_GTSRB():
    network_folder_path = "./models/GTSRB/"
    network_name = "CNN_GTSRB_0-42"
    classes = range(43)
    layers_indexes = [11, 10, 9, 8]
    taus = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0.01]
    monitors_offline_construction(network_name, network_folder_path, classes, layers_indexes, taus)

