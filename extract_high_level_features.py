#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import numpy as np 
import pandas as pd 
import tensorflow as tf
from pickle import load
from openpyxl import Workbook
from tensorflow import keras
from tensorflow.keras import layers, Sequential
from tensorflow.keras.models import Model


def get_single_layer_values(model, layer_index, x_inputs): # layer_index: an index of monitored layer
    sub_model = Model(inputs=model.input, outputs=model.layers[layer_index].output)  # sub-model until the specified layer
    result = sub_model.predict(x_inputs)    # get the outputs of vectors at the specified layer
    return result
    

def collect_high_level_features(model, layers_indexes, x_inputs, y_labels, features_storage_folder):   # layers_indexes: a list of indexes of layers
    if len(x_inputs) == len(y_labels):  # check the consistency of inputs dimensionality
        data_generated = dict() 
        data_generated["true_labels"] = np.array(y_labels)
        y_predicted = model.predict(x_inputs)   # collect predictions of the inputs
        y_predicted_labels = np.argmax(y_predicted, axis=1) 
        data_generated["predicted_labels"] = np.array(y_predicted_labels)

        for i in layers_indexes:
            features_arrary = get_single_layer_values(model, i, x_inputs)   # collect the outputs at a specified layer
            features = features_arrary.tolist()
            data_generated["features"] = features
            df = pd.DataFrame.from_dict(data_generated)
            
            feature_folder_path = features_storage_folder + "Layer_" + str(i)
             
            # check if the targeted directory exists, if not, creat that one
            if not os.path.isdir(feature_folder_path):
                os.mkdir(feature_folder_path)
            
            # classify the features by its true and predicted labels
            labels_set = set(y_predicted_labels)
            for y in labels_set:
                features_correct = df.loc[(df['true_labels']==y) & (df['predicted_labels']==y)]  # select good features correctly classified as class y
                features_correct_array = np.array(features_correct["features"].tolist())
                features_incorrect = df.loc[(df['true_labels']==y) & (df['predicted_labels']!=y)]    # select good features incorrectly classified as class y
                features_incorrect_array = np.array(features_incorrect["features"].tolist())
                # save features into a csv file
                feature_file_path = feature_folder_path + "/class_" + str(y)
                # features_correct.to_csv(new_path + "_good_high_level_features.csv", index = False)
                # features_incorrect.to_csv(new_path + "_bad_high_level_features.csv", index = False)
                np.savetxt(feature_file_path + "_good_high_level_features.csv", features_correct_array, delimiter=',')
                np.savetxt(feature_file_path + "_bad_high_level_features.csv", features_incorrect_array, delimiter=',')
    else:
        return print("The dimensionalities of inputs are not consistent.")


def collect_features_MNIST():
    # load the model
    stored_network_name = 'CNN_MNIST_0-9'
    network_folder_path = './models/MNIST/'
    model_path = "{}.h5".format(stored_network_name)
    model = keras.models.load_model(network_folder_path + model_path)

    # load the data
    mnist = tf.keras.datasets.mnist
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_train, x_test = x_train / 255.0, x_test / 255.
    x_train = x_train.reshape(x_train.shape[0], 28, 28, 1)
    x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)

    # specify the layers where patterns are extracted
    layers_indexes = [6, 7, 8, 9]

    x_inputs = x_train
    y_labels = y_train
    features_storage_folder = network_folder_path
    collect_high_level_features(model, layers_indexes, x_inputs, y_labels, features_storage_folder)


def collect_features_F_MNIST():
    # load the model
    stored_network_name = 'CNN_F_MNIST_0-9'
    network_folder_path = './models/F_MNIST/'
    model_path = "{}.h5".format(stored_network_name)
    model = keras.models.load_model(network_folder_path + model_path)

    # load the data
    f_mnist = tf.keras.datasets.fashion_mnist
    (x_train, y_train), (x_test, y_test) = f_mnist.load_data()
    x_train, x_test = x_train / 255.0, x_test / 255.
    x_train = x_train.reshape(x_train.shape[0], 28, 28, 1)
    x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)

    # specify the layers where patterns are extracted
    layers_indexes = [5, 6, 7, 8, 9]

    x_inputs = x_train
    y_labels = y_train
    features_storage_folder = network_folder_path
    collect_high_level_features(model, layers_indexes, x_inputs, y_labels, features_storage_folder)


def collect_features_CIFAR10():
    # load the model
    stored_network_name = 'CNN_CIFAR10_0-9'
    network_folder_path = './models/CIFAR10/'
    model_path = "{}.h5".format(stored_network_name)
    model = keras.models.load_model(network_folder_path + model_path)

    # load the data
    cifar10 = tf.keras.datasets.cifar10
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    x_train, x_test = x_train / 255.0, x_test / 255.
    # x_train = x_train.reshape(x_train.shape[0], 32, 32, 3)
    # x_test = x_test.reshape(x_test.shape[0], 32, 32, 3)

    # specify the layers where patterns are extracted
    layers_indexes = [8, 9, 10, 11]

    x_inputs = x_train
    y_labels = y_train.flatten()
    features_storage_folder = network_folder_path
    collect_high_level_features(model, layers_indexes, x_inputs, y_labels, features_storage_folder)


def collect_features_GTSRB():
    # load the model
    stored_network_name = 'CNN_GTSRB_0-42'
    network_folder_path = './models/GTSRB/'
    model_path = "{}.h5".format(stored_network_name)
    model = keras.models.load_model(network_folder_path + model_path)

    # load the data
    file = "./data/GTSRB/" + "train.p"
    with open(file, mode='rb') as f:
        data = load(f)
        x_train = data["features"]
        y_train = data["labels"]

    
    x_train = x_train / 255.0

    # specify the layers where patterns are extracted
    layers_indexes = [11, 10, 9, 8]

    x_inputs = x_train
    y_labels = y_train
    features_storage_folder = network_folder_path
    collect_high_level_features(model, layers_indexes, x_inputs, y_labels, features_storage_folder)


# collect_features_MNIST()

# collect_features_F_MNIST()

# collect_features_CIFAR10()

# collect_features_GTSRB()