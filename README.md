# Customizable Reference Runtime Monitoring of Neural Networks using Resolution Boxes

This repository presents the implementation and results of experiments done in our "Customizable Reference Runtime Monitoring of Neural Networks using Resolution Boxes".

## Requirements

We use Python 3.8 and following packages: numpy, jupyter, matplotlib, pandas, scikit-learn, tensorflow.

## Reproduce the results

In the following, we show how to generate and test the monitors for a network with specified data, layers and clustering parameters.

* networks: we store the networks in folder `models`;

* data: we use datesets `MNIST`, `F_MNIST`, and `CIFAR10` via functions `tensorflow.keras.datasets.mnist/F_MNIST/CIFAR10`;

* abstraction shape: `Abstractions/box.py`;

* monitors:
    - generation: notebook `NN_monitor_online_generation.ipynb`;
    - test: notebook `monitor_test_verdicts.ipynb`;
    - performance statistic: `monitors_performance_statistic.ipynb`;
    - plot comparison between uniform and customized clustering parameter: notebook `plot_comparison_uniform_vs_customized_clutering_parameter.ipynb`; 
    - plots of performance: notebook `plots_monitor_performance_results.ipynb`;
    - clustering coverage estimation: `clustering_coverage_estimation.ipynb`;




