from copy import deepcopy
import math
from scipy.spatial.distance import euclidean

#def euclidean(p1, p2):
#    return math.sqrt(sum([(a - b) ** 2 for a, b in zip(p1, p2)]))

class Sphere:
    def __init__(self):
        self.dimensions = 0
        self.radius = 0
        self.center = []
        self.sum = []
        self.n = 0
        
    def build(self, dimensions, points):
        piter = iter(points)
        self.dimensions = dimensions;
        try:
            point = next(piter)
        except StopIteration:
            self.center = [0 for _ in range(self.dimensions)]
            return
        else:
            i = 0
            for coord in point:
                if(i >= self.dimensions):
                    break
                self.sum.append(coord)
                self.center.append(coord)
                self.n += 1
                i += 1
            if(len(self.center) != self.dimensions):
                raise IllegalArgument

        while True:
            try:
                point = next(piter)
            except StopIteration:
                break
            else:
                for i, coord in enumerate(point):
                    if(i >= self.dimensions):
                        break
                    self.sum[i] += coord
                    self.center = self.sum[i] / (self.n + 1)
                d = euclidean(self.center, point)
                if(d > self.radius):
                    self.radius = d


    def query(self, point):
        return euclidean(self.center, point) < self.radius

    def __str__(self):
        return "center: " + self.ceter.__str__() + " radius: " + self.radius.__str__();
