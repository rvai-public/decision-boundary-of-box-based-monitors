#!/usr/bin/env python 3
# -*- coding: utf-8 -*-

import numpy as np 
import pandas as pd 
import math
import os.path
import time
from Abstractions import *
from sklearn.cluster import KMeans


ceil = math.ceil
# adapted ceil
def ceil(x):
    if (x % 1 == 0):
        return math.ceil(x) + 1
    else:
        return math.ceil(x)


# remove zero-length intervals from a list of intervals
def rm_zero_len_ival(ivals):
    ivals_copy = ivals[:]
    for ival in ivals:
        if ival[0] == ival[1]:
            ivals_copy.remove(ival)
    ivals = ivals_copy
    return ivals


# box intersection
def insec_box(box1, box2):
    box = Box()
    box.ivals = []
    for i in range(len(box1.ivals)):
        if (box1.ivals[i][0] > box2.ivals[i][1] or box2.ivals[i][0] > box1.ivals[i][1]):
            return "empty"
        else:
            low = max(box1.ivals[i][0], box2.ivals[i][0])
            high = min(box1.ivals[i][1], box2.ivals[i][1])
            box.ivals.append([low, high])
    return box 


# coverage by a single box
def single_coverage(size_X, sub_box, box):
    # parameter check to be added later
    coverage = 1.0
    ivals_sub = sub_box.ivals
    ivals = box.ivals
    for i, ival in enumerate(ivals_sub):
        if ival[0] == ival[1]:
            if ivals[i][0] == ivals[i][1]:
                r = 1.0
            else:
                r = (1/size_X)
        else:
            n1 = ceil(size_X * (ival[0]-ivals[i][0]) / (ivals[i][1]-ivals[i][0])) # lower bound index
            n2 = min(ceil(size_X * (ival[1]-ivals[i][0]) / (ivals[i][1]-ivals[i][0])), size_X) # upper bound index
            r = (n2 - n1 + 1)/size_X
        coverage = coverage*r 
    return coverage



# def: total coverage by a set of abstraction
def total_coverage(size_X, list_sub_boxes, box):
    total_coverage = 0.0
    for sub_box in list_sub_boxes:
        ratio = single_coverage(size_X, sub_box, box)
        total_coverage = total_coverage + ratio
    return total_coverage


# coverage_estimation 
# inputs: size_X, a set of sub-boxes, a set of pair-intersection of sub-boxes, a global box
def cov_estimation(size_X, sub_boxes, int_sub_boxex, box):
    r_high = total_coverage(size_X, sub_boxes, box)
    difference = total_coverage(size_X, int_sub_boxex, box)
    r_low = r_high - difference
    return r_low, r_high, difference


def clustering_coverage_estimation_MNIST():
    layers_indexes = [9, 8, 7, 6]
    classes = range(10)
    appendixes = ["_bad_high_level_features.csv", "_good_high_level_features.csv"]
    network_folder_path = "./models/MNIST/"
    #stored_network_name = "CNN_MNIST_0-9/"
    
    taus = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0.01]
    
    product = ((i, y, appendix) for i in layers_indexes for y in classes for appendix in appendixes)
    for i, y, appendix in product:
        start_time = time.time()
        # load data for class y at layer i
        features_file_path = network_folder_path +"Layer_" + str(i) + "/class_" + str(y) + appendix
        X = np.genfromtxt(features_file_path, delimiter=',')

        # build a global box on X
        n_dim = X.shape[1]
        g_box = Box()
        g_box.build(n_dim, X)
        #print(len(g_box.ivals))

        # construct covered box for X, named rg_box, reduced global box
        rg_box = Box()
        rg_box.ivals = rm_zero_len_ival(g_box.ivals)
        n_dim_cov = len(rg_box.ivals) # dimension of the covered space
        # print(len(rg_box.ivals))
        # print(rg_box.ivals)

        # load existing clustering results
        clustering_results_path = network_folder_path + "Layer_" + str(i) + "/clustering_results_class_" + str(y) + appendix
        clustering_results = pd.read_csv(clustering_results_path)

        CC_estimation = [] # for storing the results
        CC_estimation_path = network_folder_path + "Layer_" + str(i) + "/CC_estimation_class_" + str(y) + appendix

        # estimate clustering coverage for each tau
        for tau in taus:
            clustering_result = clustering_results[str(tau)] # load clustering result related to tau      
            # k = 1
            # kmeans = KMeans(n_clusters=k, random_state=0).fit(X)
            # clustering_result = kmeans.labels_

            # partition the given dataset according clustering results and build a list of local boxes
            
            # determine the labels of clusters
            num_clusters = np.amax(clustering_result) + 1
            clustering_labels = np.arange(num_clusters)
            # extract the indices of vectors in a cluster
            clusters_indices = []
            for k in clustering_labels:
                indices_cluster_k, = np.where(clustering_result == k)
                clusters_indices.append(indices_cluster_k)
                
            # creat local box for each cluster
            loc_boxes = [Box() for i in clustering_labels]
            for j in range(len(loc_boxes)):
                loc_boxes[j].build(n_dim, X[clusters_indices[j]])


            # do pair-wise instersection of local boxes
            int_loc_boxes = []
            for i in range(len(loc_boxes)):
                for j in range(i+1, len(loc_boxes)):
                    overlap = insec_box(loc_boxes[i], loc_boxes[j])
                    if not (overlap == "empty"):
                        int_loc_boxes.append(overlap)
            num_int_boxes = len(int_loc_boxes)
            
            # print(len(int_loc_boxes))
            # if len(int_loc_boxes)>0:
            #     print(int_loc_boxes[0])

            size_dataset = X.shape[0]
            low, high, diff = cov_estimation(size_dataset, loc_boxes, int_loc_boxes, g_box)
            
            # record 6 elements: tau, number of clusters, number of inersected boxes, lower and upper bounds, bound difference
            history = [tau, num_clusters, num_int_boxes, low, high, diff]
            CC_estimation.append(history)
        
        df = pd.DataFrame(CC_estimation, columns=['tau', 'n_clusters', 'n_int_boxes', 'lower_bound', 'high_bound', 'bound_diff'])
        df.to_csv(CC_estimation_path, index = False)

        elapsed_time = time.time() - start_time
        print(clustering_results_path + ",", "lasting time:", elapsed_time, "seconds")


# clustering_coverage_estimation_MNIST()